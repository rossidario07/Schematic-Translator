# Schematic Translator

Simple and quickly app to transform your .schematic file in .obj file

## Getting Started

These instructions will you get you the requirements to run and install Schematic translator

### Prerequisites

To run Schematic Translator you need Java 8 or above installed

If you haven't it follow the installation process [here](https://gitlab.com/rossidario07/Schematic-Translator/blob/master/REQUIREMENTS.md)

### Installing

Schematic Translator is downloadable on the website
Older version can found here

## Deployment

Add additional notes about how to deploy this on a live system

## Authors

* **Andrea Rossi** - *Initial work* - [Blovien](https://gitlab.com/rossidario07)

## License

This project is licensed under the Apache License - see the [LICENSE](LICENSE) file for details