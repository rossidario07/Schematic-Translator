# Requirements and installation

### Windows

Go [here](https://java.com/en/download/) and download it

### Mac

* Terminal installation :

First install and update Brew :

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew tap caskroom/versions

brew update
```

If you want to install Java 8 type :

```
brew cask install java8
```
If you want to install the latest version type :

```
brew cask install java
```

* Normal installation :

Like Windows go [here](https://java.com/en/download/).

### Linux

On linux you have to install two things:
* Java 8 or above
* ```openjfx```

To install Java 8 first check if you have an older version of java than 8

```
java -version
```

else, type :

```
sudo apt-get update

sudo apt-get install default-jre
```

or if you want to install the Oracle JDK, which is the official version distributed by Oracle, you will need to follow a few more steps.

```
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update

sudo apt-get install oracle-java8-installer ( for java 8 )

sudo apt-get install oracle-java9-installer ( for java 9 etc...)
```
